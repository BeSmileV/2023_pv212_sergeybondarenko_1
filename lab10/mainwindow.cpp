#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "FileName/filename.h"

#include <QFileDialog>
#include <QStringList>
#include <QString>
#include <string>
#include <iostream>
#include <fstream>

QStringList fileNames;
QString selectedFolder;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

// Кнопка для выбора файлов, которые будут скопированы
void MainWindow::on_pushButton_clicked()
{
    fileNames = QFileDialog::getOpenFileNames(
                this,
                tr("Open Files"),
                QDir::homePath(),
                tr("Text Files (*.txt);;All Files (*.*)"),
                nullptr,
                QFileDialog::Option::ReadOnly | QFileDialog::Option::DontUseNativeDialog
                );
    ui->textBrowser->append("Выбранные файлы :\n");
    for(const auto &x : fileNames) {
        ui->textBrowser->append(x);
        ui->textBrowser->append("\n");
    }
}

// Кнопка для выбора конечной директории
void MainWindow::on_pushButton_2_clicked()
{
    selectedFolder = QFileDialog::getExistingDirectory(nullptr, "Выберите папку", QDir::homePath());
    ui->textBrowser_2->append("Выбранная папка :\n");
    ui->textBrowser_2->append(selectedFolder);
}

void MainWindow::on_pushButton_3_clicked()
{
 ui->textBrowser_3->append("Скопированные файлы :\n");
    for(const auto &x : fileNames) {
        FileName wayToCopyFile(x.toStdString());
        FileName wayToCopiedFile(wayToCopyFile.getName(), selectedFolder.toStdString());

        // Открываем исходный файл для чтения
        std::ifstream source(wayToCopyFile.getFullWay(), std::ios::binary);

        // Открываем файл для записи
        std::ofstream dest(wayToCopiedFile.getFullWay(), std::ios::binary);

        // Копируем содержимое исходного файла в файл назначения
        dest << source.rdbuf();

        // Закрываем файлы
        source.close();
        dest.close();

        ui->textBrowser_3->append(QString::fromStdString(wayToCopiedFile.getFullWay()));
        ui->textBrowser_3->append("\n");
    }
}
