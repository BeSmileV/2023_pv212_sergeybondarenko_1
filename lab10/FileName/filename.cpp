#include "filename.h"

FileName::FileName(const std::string &str_fullWay) : fullWay(str_fullWay){
    int i = static_cast<int>(str_fullWay.size()) - 1;

    while(i >= 0 && str_fullWay[i] != '/') {
        name.insert(name.begin(), str_fullWay[i]);
        i--;
    }
}

FileName::FileName(const std::string &str_name, const std::string &str_otherPart) : name(str_name){
    fullWay = str_otherPart;
    fullWay += '/';
    fullWay += str_name;
}

std::string FileName::getName() {
    return name;
}

std::string FileName::getFullWay() {
    return fullWay;
}
