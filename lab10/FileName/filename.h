#ifndef FILENAME_H
#define FILENAME_H

#include <string>

class FileName {
private:
    std::string name;
    std::string fullWay;
public:
    FileName(const std::string &str_fullWay);

    FileName(const std::string &str_name, const std::string &str_otherPart);

    std::string getName();

    std::string getFullWay();
};

#endif // FILENAME_H
